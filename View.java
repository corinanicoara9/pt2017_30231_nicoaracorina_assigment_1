package poli1;

import java.awt.event.ActionEvent;
public class View {

    {
        ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
        ServiceReference2.ServiceConsultationClient serverConsult = new ServiceReference2.ServiceConsultationClient();
        ServiceReference3.ServicePacientiClient servicePacient = new ServiceReference3.ServicePacientiClient();


        public DataSet getTabelPacienti()
        {
            return servicePacient.viewTabelPacient();
        }

        public DataSet getTabelPacientNume(String nume)
        {
            return servicePacient.viewTabelPacientNume(nume);
        }

        public void addPacient(Pacient pacient)
        {
            try
            {
                servicePacient.addPacient(pacient);
            }
            catch (NotImplementedException notImp)
            {
                Console.WriteLine(notImp.Message);
            }
        }

        public void updatePacient(Pacient pacient)
        {
            try
            {
                servicePacient.updatePacient(pacient);
            }
            catch (NotImplementedException notImp)
            {
                Console.WriteLine(notImp.Message);
            }
        }

    }
}


}
