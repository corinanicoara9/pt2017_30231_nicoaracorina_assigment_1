package poli1;
import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import poli.Monom;
import poli.Operatii;
import poli.Polinom;

public class Test {

    public partial class Login : Form
    {
        Administrator adm;
        
        Secretary secretara;
        public Login()
        {
            InitializeComponent();
            adm = new Administrator();
            secretara = new Secretary();
           
            
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Accounts ac=new Accounts();
            String user = textBoxUsername.Text;
            String pass = textBoxPassword.Text;
            if (user != "" && pass != "")
            {
                if (ac.getType(user, pass).Equals("Administrator"))

                    adm.Show();
                else if (ac.getType(user, pass).Equals("Doctor"))
                {
                    Doctori doctori = new Doctori(user);
                    secretara.attachObserver(doctori);
                    doctori.Show();
                }
                else if (ac.getType(user, pass).Equals("Secretary"))
                {
                    Secretary s = new Secretary();
                    s.Show();
                }
                else
                    MessageBox.Show("Nu exista!");

            }
            else MessageBox.Show("Introduceti datele!");
        }
    }
}

}
