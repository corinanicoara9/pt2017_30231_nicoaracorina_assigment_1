package poli1;

import java.util.*;

public class Polinom {


    public partial class Secretary : Form,Observable
    {
        private static  List<Observer> observers = new List<Observer>();
        Accounts acount;
        Consultatii consultatii;
        PacientService pacientser;
        public Secretary()
        {
            //panel1.Visible = false;
            //panelConsultati.Visible = false;
            InitializeComponent();
            acount=new Accounts();
            consultatii=new Consultatii();
            pacientser=new PacientService();
        }
        public void attachObserver(Observer o)
        {
            Console.WriteLine("am adaugat");
            observers.Add(o);
        }

        public void detachObserver(Observer o)
        {
            observers.Remove(o);
        }

        public void notifyObservers(String text)
        {
            foreach (var o in observers)
            {
                String text1 = "A venit pacientul " + textBoxPacName.Text;
                    o.update(text1,text);
                
            }
        }

        private void Secretary_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'clinicaDataSet4.consultations' table. You can move, or remove it, as needed.
           // this.consultationsTableAdapter.Fill(this.clinicaDataSet4.consultations);
            // TODO: This line of code loads data into the 'clinicaDataSet3.patients' table. You can move, or remove it, as needed.
           // this.patientsTableAdapter.Fill(this.clinicaDataSet3.patients);

        }

        private void buttonViewPacient_Click(object sender, EventArgs e)
        {
            DataSet ds = pacientser.getTabelPacienti();
            DataTable dt = new DataTable();
            dt = ds.Tables["patients"];
            dataGridViewPacient.DataSource = dt;
        }

        private void pacientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //panelPatient.Visible = true;
            //panelConsultati.Visible = false;
        }

        private void consultationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //panelPatient.Visible = false;
            //panelConsultati.Visible = true;
        }

        private void buttonAddPAcient_Click(object sender, EventArgs e)
        {
            String[] str = new String[6];
            int row = dataGridViewPacient.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = dataGridViewPacient.Rows[row].Cells[i].Value.ToString();
            }
            Pacient s = new Pacient();
            s.NrCard = Convert.ToInt32(str[0]);
            s.NumePacient1 = str[1];
            s.NrCard = Convert.ToInt32(str[2]);
            s.Cnp = str[3];
            s.DateBirth = str[4];
            s.Adress = str[5];
            pacientser.addPacient(s);
        }

        private void buttonUpdatePacient_Click(object sender, EventArgs e)
        {
            String[] str = new String[6];
            int row = dataGridViewPacient.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = dataGridViewPacient.Rows[row].Cells[i].Value.ToString();
            }
            Pacient s = new Pacient();
            s.NrCard = Convert.ToInt32(str[0]);
            s.NumePacient1 = str[1];
            s.NrCard = Convert.ToInt32(str[2]);
            s.Cnp = str[3];
            s.DateBirth = str[4];
            s.Adress = str[5];
            pacientser.updatePacient(s);
        }



}
