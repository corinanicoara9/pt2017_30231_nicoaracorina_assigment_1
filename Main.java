package poli1;
import java.util.*;
public class Main {

    public interface Observable
    {
        void attachObserver(Observer o);
        void detachObserver(Observer o);
        void notifyObservers(String text);
    }

}
