package poli1;

public class Monom {


    public partial class Administrator : Form
    {
        Accounts acount;
        public Administrator()
        {
            InitializeComponent();
            panelDoctori.Visible = false;
            panelSecretary.Visible = false;
            acount =new Accounts();
        }

        private void Administrator_Load(object sender, EventArgs e)
        {
            
        }

        private void cRUDDoctorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelDoctori.Visible = true;
            panelSecretary.Visible = false;
        }

        private void cRUDSecretaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelDoctori.Visible = false;
            panelSecretary.Visible = true;
        }

        private void buttonAddSecretary_Click(object sender, EventArgs e)
        {
            String[] str = new String[4];
            int row = dataGridViewSecretary.CurrentRow.Index;
            for (int i = 0; i < 4; i++)
            {
                str[i] = dataGridViewSecretary.Rows[row].Cells[i].Value.ToString();
            }
            Secretara s = new Secretara();
            s.Id = Convert.ToInt32(str[0]);
            s.Nume = str[1];
            s.Username = str[2];
            s.Password = str[3];
           User u = new User();
           u.Username = str[2];
           u.Password = str[3];
           u.Type = "Secretary";
            acount.adaugaSecretara(s,u);

        }

        private void buttonUpdateSecretary_Click(object sender, EventArgs e)
        {
            String[] str = new String[4];
            int row = dataGridViewSecretary.CurrentRow.Index;
            for (int i = 0; i < 4; i++)
            {
                str[i] = dataGridViewSecretary.Rows[row].Cells[i].Value.ToString();
            }
            Secretara s = new Secretara();
            s.Id = Convert.ToInt32(str[0]);
            s.Nume = str[1];
            s.Username = str[2];
            s.Password = str[3];
            User u = new User();
            u.Username = str[2];
            u.Password = str[3];
            u.Type = "Secretary";
            acount.updateSecretara(s, u);

        }

        private void buttonDeleteSecretary_Click(object sender, EventArgs e)
        {
            String[] str = new String[4];
            int row = dataGridViewSecretary.CurrentRow.Index;
            for (int i = 0; i < 4; i++)
            {
                str[i] = dataGridViewSecretary.Rows[row].Cells[i].Value.ToString();
            }
            Secretara s = new Secretara();
            s.Id = Convert.ToInt32(str[0]);
            s.Nume = str[1];
            s.Username = str[2];
            s.Password = str[3];
            User u = new User();
            u.Username = str[2];
            u.Password = str[3];
            u.Type = "Secretary";
            acount.deleteSecretara(s, u);

        }

        private void buttonSearchSecretary_Click(object sender, EventArgs e)
        {
            if (textBoxSecretaryName.Text != "")
            {
                DataSet ds = acount.afisareSecretara(textBoxSecretaryName.Text);
                DataTable dt = new DataTable();
                dt = ds.Tables["secretary"];
                dataGridViewSecretary.DataSource = dt;
               // dataGridViewSecretary.DataSource = acount.afisareSecretara(textBoxSecretaryName.Text);
            }
            else MessageBox.Show("Introduceti numele!");

        }

        private void buttonViewSecretary_Click(object sender, EventArgs e)
        {
            DataSet ds = acount.afisareTabelSecretara();
            DataTable dt = new DataTable();
            dt = ds.Tables["secretary"];
            dataGridViewSecretary.DataSource = dt;
           
        }

        private void buttonView_Click(object sender, EventArgs e)
        {

            DataSet ds = acount.afisareTabelDoctor();
            DataTable dt = new DataTable();
            dt = ds.Tables["doctors"];
            dataGridViewDoctor.DataSource = dt;

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            String[] str = new String[4];
            int row = dataGridViewDoctor.CurrentRow.Index;
            for (int i = 0; i < 4; i++)
            {
                str[i] = dataGridViewDoctor.Rows[row].Cells[i].Value.ToString();
            }
            Doctor s = new Doctor();
            s.Id = Convert.ToInt32(str[0]);
            s.Nume = str[1];
            s.Username = str[2];
            s.Password = str[3];
            User u = new User();
            u.Username = str[2];
            u.Password = str[3];
            u.Type = "Doctor";
            acount.deleteDoctor(s, u);

        }


      
        }

}
