package poli1;

public class MonomDouble {


    public partial class Doctori : Form,Observer
    {
        Accounts account;
        Consultatii consultatii;
        String name;

        public String Name1
        {
            get { return name; }
            set { name = value; }
        }

        public Doctori(String _name)
        {
            this.name = _name;
            account = new Accounts();
            consultatii = new Consultatii();
            InitializeComponent();
        }
        public void update(String s,String user)
        {
            Console.WriteLine("fac update");
            if (this.name.Equals(user))
            {
                Console.WriteLine("is good");
                textBoxArrivePacient.Text = s;
            }
        }

        private void Doctori_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'clinicaDataSet2.consultations' table. You can move, or remove it, as needed.
           // this.consultationsTableAdapter.Fill(this.clinicaDataSet2.consultations);

        }

        private void buttonGetPacientId_Click(object sender, EventArgs e)
        {
            if (textBoxPacientName.Text != "")
            {
                textBoxPacientId.Text = account.getIdPacient(textBoxPacientName.Text).ToString();
            }
            else
                MessageBox.Show("Introduceti numele pacientului");
        }

        private void buttonGetIdDoctor_Click(object sender, EventArgs e)
        {
            if (textBoxDoctorName.Text != "")
            {
                textBoxIdDoctor.Text = account.getIdDoctor(textBoxDoctorName.Text).ToString();
            }
            else
                MessageBox.Show("Introduceti numele doctorului!");
        }

        private void buttonAddConsult_Click(object sender, EventArgs e)
        {
            String[] str = new String[4];
            int row = dataGridViewConsultatii.CurrentRow.Index;
            for (int i = 0; i < 4; i++)
            {
                str[i] = dataGridViewConsultatii.Rows[row].Cells[i].Value.ToString();
            }
            Consultation  s = new Consultation();
            s.Id_consultation = Convert.ToInt32(str[0]);
            s.Id_pacient = Convert.ToInt32(str[1]);
            s.Date = str[2];
            s.Id_doctor = Convert.ToInt32(str[3]);
            consultatii.adaugaConsultatie(s);
        }

        private void buttonViewConsultPacient_Click(object sender, EventArgs e)
        {
            if (textBoxPacientId.Text != "")
            {
                DataSet ds = consultatii.viewConsultPacientBuId(Convert.ToInt32(textBoxPacientId.Text));
                DataTable dt = new DataTable();
                dt = ds.Tables["consultations"];
                dataGridViewConsultatii.DataSource = dt;
 
            }
        }
    }
}

}
